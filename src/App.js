import React, { Component } from "react";
import todosList from "./todos.json";
import uuid from 'react-uuid'

class App extends Component {
  state = {
    todos: todosList,
    newItem: {}
  };

  newItems = e => {
    this.setState({newItem: {userId: 1, id: uuid(), title: e.target.value, completed:false}})
  }
  onEnter = event => {
      if(event.keyCode === 13){
      this.setState({
        todos: [...this.state.todos,this.state.newItem],
      })
      event.target.value = ''
    }}
    completeTodo = e => {
    const completeArr =  this.state.todos.map(items => {
        if (items.title === e)
        items.completed = !items.completed
        return items
      })
      this.setState({
        todos: completeArr
      })
    }

    deleteTodo = e => {
      const deleteObj = this.state.todos.filter(items => {
       return e !== items.title
      })
      this.setState({
        todos: deleteObj
      })
    }

    clearAll = e => {
      const clearObj = this.state.todos.filter(items => {
        return false === items.completed
      })
      this.setState({
        todos: clearObj
      })
    }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" onChange={this.newItems} onKeyDown={this.onEnter} autoFocus/>
        </header>
        <TodoList 
        todos={this.state.todos} 
        completeTodo={this.completeTodo}
        deleteTodo={this.deleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearAll}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onChange={() =>
            this.props.completeTodo(this.props.title)}/>
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() =>  
            this.props.deleteTodo(this.props.title)
            }/>
        </div>
      </li>
    );
  }
}



class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem  
            title={todo.title} 
            completed={todo.completed} 
            key={todo.id}
            completeTodo={this.props.completeTodo}
            deleteTodo={this.props.deleteTodo}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
